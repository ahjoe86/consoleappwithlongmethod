﻿using ConsoleAppWithLongMethod.Model;
using System;

namespace ConsoleAppWithLongMethod
{
    class Program
    {
        static void ThisIsMethodWithVeryLongNameWithParametersThatShiftingTheScreen(SampleLongNameModel param1, SampleLongNameModel param2, SampleLongNameModel param3, SampleLongNameModel param4, SampleLongNameModel param5, SampleLongNameModel param6, SampleLongNameModel param7, SampleLongNameModel param8)
        {

        }

        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }
    }
}
